package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne1;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main2 {

    public static void main(String[] args) {

        Araba[] arabalar = new Araba[10];

        for (int i = 0; i < 10; i++) {

            Araba araba = new Araba();
            araba.marka = "Araba Markası " + i;
            araba.model = "Araba Modeli " + i;
            araba.vitesSayisi = 10 + i;
            araba.sonHiz = 200 + (i * 10);

            arabalar[i] = araba;

            System.out.println(arabalar[i]);
//            System.out.println(araba.toString());
        }

        System.out.println("Tüm arabalar yaratıldı.");

        arabalar[2].git(15);

        arabalar[4].git(2);

        arabalar[0].marka = "LOTUS";
        arabalar[2].marka = "MERCEDES";
        arabalar[6].marka = "TOYOTA";
        arabalar[7].marka = "BMW";

        System.out.println(arabalar[0]);

        System.out.println();
        System.out.println();


        for (int i = 0; i < arabalar.length; i++) {

            if (i % 2 == 0) {

                arabalar[i].model = "Çift sayı modeli " + i;
            }
        }


        for (int i = 0; i < arabalar.length; i++) {

            System.out.println(arabalar[i]);
        }

    }
}
