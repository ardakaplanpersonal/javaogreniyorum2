package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne6;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Doktor extends Kisi {

    private String uzmanlikYili;

    public String getUzmanlikYili() {
        return uzmanlikYili;
    }

    public void setUzmanlikYili(String uzmanlikYili) {
        this.uzmanlikYili = uzmanlikYili;
    }

    @Override
    public void ekranaYazdir() {

        System.out.println("Merhaba ben bir doktorum.");
    }

    @Override
    public String toString() {
        return "Doktor{" +
                "uzmanlikYili='" + uzmanlikYili + '\'' + super.toString() +
                '}';
    }
}
