package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

//        arrayList();

//        map();

        set();
    }

    public static void set() {

        HashSet<Sebze> sebzeler = new HashSet<>();

        Brokoli brokoli = new Brokoli("orjinal brokoli");

        sebzeler.add(brokoli);
        sebzeler.add(brokoli);
        sebzeler.add(brokoli);
        sebzeler.add(brokoli);
        sebzeler.add(brokoli);


        sebzeler.add(new Lahana("lahana1"));
        sebzeler.add(new Lahana("lahana2"));
        sebzeler.add(new Brokoli("brokoli1"));
        sebzeler.add(new Brokoli("brokoli2"));
        sebzeler.add(new Brokoli("brokoli2"));

        for (Sebze sebze : sebzeler) {

            System.out.println(sebze);
        }

    }


    public static void map() {

        HashMap<String, Brokoli> map = new HashMap<>();

        map.put("arda", new Brokoli("brokoli 1 "));
        map.put("lahana", new Brokoli("brokoli 2 "));
        map.put("elma", new Brokoli("brokoli 3 "));
        map.put("cicek", new Brokoli("brokoli 4 "));


        System.out.println(map.get("cicek"));

    }


    public static void arrayList() {


        ArrayList<Lahana> lahanaArrayList = new ArrayList<>();

        for (int i = 0; i < 50; i++) {

            Lahana lahana = new Lahana("Lahana " + i);
            lahanaArrayList.add(lahana);
        }

        Lahana lahana = new Lahana("orjinal lahana");

        lahanaArrayList.add(4, lahana);


        lahanaArrayList.remove(45);


        for (int i = 0; i < lahanaArrayList.size(); i++) {

            System.out.println(lahanaArrayList.get(i));
        }

        lahanaArrayList.remove(lahana);

        System.out.println();
        System.out.println();

        for (int i = 0; i < lahanaArrayList.size(); i++) {

            System.out.println(lahanaArrayList.get(i));
        }
    }
}
