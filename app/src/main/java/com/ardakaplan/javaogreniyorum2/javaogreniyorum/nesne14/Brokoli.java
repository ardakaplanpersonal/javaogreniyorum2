package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne14;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Brokoli extends Sebze {

    public void brokoliMetodu() {

        System.out.println("brokoliMetodu çalıştı.");
    }

    public Brokoli(String isim) {
        super(isim);
    }


}
