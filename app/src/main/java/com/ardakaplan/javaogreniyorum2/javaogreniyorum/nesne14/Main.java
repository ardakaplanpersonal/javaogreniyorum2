package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne14;

import java.util.ArrayList;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        ArrayList<Sebze> sebzeler = new ArrayList<>();

        for (int i = 0; i < 100; i++) {

            sebzeler.add(new Brokoli("brokoli " + i));
        }

        //kod bu satıra gelince içinde 100 tane brokoli bulunan sebze listem var


        ((Brokoli) sebzeler.get(4)).brokoliMetodu();

    }


}
