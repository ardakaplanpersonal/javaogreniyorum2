package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne4;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Doktor {

    private String ismi;
    private String uzmanligi;

    public Doktor(String ismi, String uzmanligi) {
        this.ismi = ismi;
        this.uzmanligi = uzmanligi;
    }

    public Doktor(String ismi) {
        this.ismi = ismi;
    }

    public Doktor() {

    }


    public String getIsmi() {
        return ismi;
    }

    public void setIsmi(String ismi) {
        this.ismi = ismi;
    }

    public String getUzmanligi() {
        return uzmanligi;
    }

    public void setUzmanligi(String uzmanligi) {
        this.uzmanligi = uzmanligi;
    }

    @Override
    public String toString() {
        return "Doktor{" +
                "ismi='" + ismi + '\'' +
                ", uzmanligi='" + uzmanligi + '\'' +
                '}';
    }
}
