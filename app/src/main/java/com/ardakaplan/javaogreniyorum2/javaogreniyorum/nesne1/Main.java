package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne1;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Araba araba1 = new Araba();

        araba1.marka = "Toyota";
        araba1.model = "Corolla";
        araba1.sonHiz = 180;
        araba1.vitesSayisi = 5;

        System.out.println(araba1.toString());

        araba1.git(3);

        System.out.println();
        System.out.println();

        Araba araba2 = new Araba();

        araba2.marka = "Renault";
        araba2.model = "Megane";
        araba2.sonHiz = 200;
        araba2.vitesSayisi = 7;


        System.out.println(araba2);
        araba2.git(3);


//        System.out.println(araba1);
//
//
//        System.out.println(araba1.marka);
//        System.out.println(araba1.model);
//        System.out.println(araba1.sonHiz);
//        System.out.println(araba1.vitesSayisi);


    }
}
