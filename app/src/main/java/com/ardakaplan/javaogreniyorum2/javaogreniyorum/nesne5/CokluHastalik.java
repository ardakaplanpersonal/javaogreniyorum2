package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne5;

import com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne4.Doktor;

import java.util.Arrays;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class CokluHastalik {

    private String isim;
    private Doktor[] doktorlar;


    public CokluHastalik(String isim) {
        this.isim = isim;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public Doktor[] getDoktorlar() {
        return doktorlar;
    }

    public void setDoktorlar(Doktor[] doktorlar) {
        this.doktorlar = doktorlar;
    }

    @Override
    public String toString() {
        return "CokluHastalik{" +
                "isim='" + isim + '\'' +
                ", doktorlar=" + Arrays.toString(doktorlar) +
                '}';
    }
}
