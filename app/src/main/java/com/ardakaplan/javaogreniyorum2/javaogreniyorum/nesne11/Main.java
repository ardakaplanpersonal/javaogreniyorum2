package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne11;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    /*
    final kelimesinin özellikleri

    1_ sınıf ismi başına konulursa, o sınıf extend edilmez olur
    2_ değişkenin önüne gelirse, o değişkeni değişmez yapar
    3_ metodun önüne gelirse o metot override edilemez olur
    */

    private static final int EHLIYET_ALMA_YASI = 18;

    public static void main(String[] args) {

//        EHLIYET_ALMA_YASI = 20;

        System.out.println(EHLIYET_ALMA_YASI);
    }
}
