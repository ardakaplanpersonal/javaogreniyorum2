package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne7;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class MotorluTasit extends Tasit {

    private int motorHacmi;

    public int getMotorHacmi() {
        return motorHacmi;
    }

    public void setMotorHacmi(int motorHacmi) {
        this.motorHacmi = motorHacmi;
    }

    @Override
    public String toString() {
        return "MotorluTasit{" +
                "motorHacmi=" + motorHacmi + super.toString() +
                '}';
    }
}
