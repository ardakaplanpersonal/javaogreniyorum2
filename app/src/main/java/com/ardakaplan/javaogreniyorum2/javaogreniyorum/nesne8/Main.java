package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne8;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Bitki lahana = new Bitki(50, "lahana");
        Hayvan kopek = new Hayvan(50, "köpek");
//        Canli fil = new Canli(300, "fil");

        System.out.println(lahana);
        System.out.println(kopek);
//        System.out.println(fil);

        lahana.buyumek();

        kopek.buyumek();

        System.out.println(lahana);
        System.out.println(kopek);
    }
}
