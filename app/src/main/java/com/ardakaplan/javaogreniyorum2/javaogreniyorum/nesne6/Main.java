package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne6;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Kisi kisi = new Kisi();
        kisi.setIsim("Arda Kaplan");
        kisi.setTcKimlikNo(45887);
        System.out.println(kisi);

        kisi.ekranaYazdir();

        Doktor doktor = new Doktor();
        doktor.setIsim("Ahmet Can");
        doktor.setTcKimlikNo(123456799);
        doktor.setUzmanlikYili("5");
        System.out.println(doktor);

        doktor.ekranaYazdir();

        Avukat avukat = new Avukat();
        avukat.setIsim("Ali Veli");
        avukat.setTcKimlikNo(4587);
        avukat.setBakdıgıDavaSayisi(5);
        System.out.println(avukat);

        avukat.ekranaYazdir();

    }
}
