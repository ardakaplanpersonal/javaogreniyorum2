package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne7;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Araba extends MotorluTasit {

    private String tipi;

    public String getTipi() {
        return tipi;
    }

    public void setTipi(String tipi) {
        this.tipi = tipi;
    }

    @Override
    public boolean equals(Object obj) {


        return getMarka().equals(((Araba) obj).getMarka());
    }

    @Override
    public void herhangiBirSeyYapanMethod() {

        System.out.println("BEN ARABADAN GELDIM");

    }

    @Override
    public String toString() {
        return "Araba{" +
                "tipi='" + tipi + '\'' + super.toString() +
                '}';
    }
}
