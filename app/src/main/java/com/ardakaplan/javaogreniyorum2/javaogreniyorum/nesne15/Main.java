package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne15;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        new Thread(new Runnable() {

            @Override
            public void run() {

                for (int i = 0; i < 100; i++) {

                    System.out.println("BBBBBB " + i);
                }

            }
        }).start();

        for (int i = 0; i < 100; i++) {

            System.out.println("AAAAAA " + i);
        }

    }
}
