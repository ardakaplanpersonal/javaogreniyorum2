package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne18;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Kisi {

    private String isim;

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public void isimKontrol() {

        if (isim.length() < 5) {

//            throw new KisaIsimHatasi();

            throw new RuntimeException("KISA ISIM HATASI");

        } else {

            System.out.println("GÜZEL İSİM");
        }
    }


    public static void main(String[] args) {

        Kisi kisi = new Kisi();
        kisi.setIsim("ARDA");

        try {

            kisi.isimKontrol();

        } catch (RuntimeException runTimeExcepiton) {

            runTimeExcepiton.printStackTrace();

        }


    }
}
