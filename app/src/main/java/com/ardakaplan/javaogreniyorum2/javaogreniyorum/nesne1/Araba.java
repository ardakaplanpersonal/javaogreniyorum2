package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne1;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Araba {

    public String marka;
    public String model;
    public int sonHiz;
    public int vitesSayisi;


    public void git(int vites) {

        if (vites > vitesSayisi) {

            System.out.println("Arabanın mevcut maksimum vites sayısı :" + vitesSayisi + ". Bu vitesin üzerinde bir rakam veremezsiniz");

        } else {

            float viteseBagliMaksimumHiz = (float) sonHiz / vitesSayisi;

            System.out.println(vites + ". viteste en fazla " + (viteseBagliMaksimumHiz * vites) + " la gidebilirsiniz.");
        }
    }

    @Override
    public String toString() {
        return "Araba{" +
                "marka='" + marka + '\'' +
                ", model='" + model + '\'' +
                ", sonHiz=" + sonHiz +
                ", vitesSayisi=" + vitesSayisi +
                '}';
    }
}
