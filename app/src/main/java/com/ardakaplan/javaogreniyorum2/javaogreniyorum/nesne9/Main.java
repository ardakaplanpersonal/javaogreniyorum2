package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne9;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Insan insan = new Insan();

        insan.setIsim("Buse");

        insan.yuru(55);

        insan.yemekYe();
    }
}
