package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne9;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public interface YemekYer {

     void yemekYe();
}
