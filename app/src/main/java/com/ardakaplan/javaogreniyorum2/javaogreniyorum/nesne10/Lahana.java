package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne10;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Lahana extends Sebze {

    public Lahana(String isim) {
        super(isim);
    }
}
