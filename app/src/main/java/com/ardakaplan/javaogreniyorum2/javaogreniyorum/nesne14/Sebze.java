package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne14;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public abstract class Sebze {

    private String isim;

    public Sebze(String isim) {
        this.isim = isim;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    @Override
    public String toString() {
        return "Sebze{" +
                "isim='" + isim + '\'' +
                '}';
    }
}
