package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne5;

import com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne4.Doktor;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Hasta {

    private String isim;
    private Doktor doktor;


    public Hasta(String isim) {
        this.isim = isim;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public Doktor getDoktor() {
        return doktor;
    }

    public void setDoktor(Doktor doktor) {
        this.doktor = doktor;
    }

    @Override
    public String toString() {
        return "Hasta{" +
                "isim='" + isim + '\'' +
                ", doktor=" + doktor +
                '}';
    }
}
