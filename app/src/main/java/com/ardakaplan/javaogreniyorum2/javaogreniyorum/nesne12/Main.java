package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne12;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {


    public static void main(String[] args) {

        System.out.println(Not.PEK_IYI);

        System.out.println(Not.IYI.getRakamNotu());

        System.out.println(Not.KOTU.getYazi());
    }
}
