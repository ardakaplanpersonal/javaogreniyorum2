package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne7;

import java.util.Arrays;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Kisi {

    private String isim;
    private Tasit[] tasitlar;

    public Kisi(String isim) {
        this.isim = isim;
    }

    public String getIsim() {
        return isim;
    }

    public Tasit[] getTasitlar() {
        return tasitlar;
    }

    public void setTasitlar(Tasit[] tasitlar) {
        this.tasitlar = tasitlar;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    @Override
    public String toString() {
        return "Kisi{" +
                "isim='" + isim + '\'' +
                ", tasitlar=" + Arrays.toString(tasitlar) +
                '}';
    }
}


