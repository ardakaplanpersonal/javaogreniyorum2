package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne8;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public abstract class Canli {

    private int kutle;
    private String isim;

    public Canli(int kutle, String isim) {
        this.kutle = kutle;
        this.isim = isim;
    }

    public abstract void buyumek();

    public int getKutle() {
        return kutle;
    }

    public void setKutle(int kutle) {
        this.kutle = kutle;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    @Override
    public String toString() {
        return "Canli{" +
                "kutle=" + kutle +
                ", isim='" + isim + '\'' +
                '}';
    }
}
