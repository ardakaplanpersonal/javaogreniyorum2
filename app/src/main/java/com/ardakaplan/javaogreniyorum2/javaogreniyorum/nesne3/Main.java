package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne3;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        //int,  Integer
        // float, Float
        // long, Long
        // double, Double
        // byte, Byte
        // boolean, Boolean
        // short Short


        String sayiString = "500";

        int sayi = Integer.valueOf(sayiString);

        System.out.println((sayi + 5));

        System.out.println();

        ////////////////////////

        String isim = "Arda Kaplan";

        System.out.println(isim.length());
        System.out.println(isim.contains("mehmet"));
        System.out.println(isim.substring(2, 8));

        isim = isim.replace("a", "e");

        System.out.println(isim);

        System.out.println(isim.charAt(9));

        ///////

        int sayii = 5;

        Integer sayiObjesi = 4;


    }
}
