package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne11;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public abstract class ElektrikliCihaz {

    private String marka;

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public final void ekranaYazdir() {
    }

    @Override
    public String toString() {
        return "ElektrikliCihaz{" +
                "marka='" + marka + '\'' +
                '}';
    }
}
