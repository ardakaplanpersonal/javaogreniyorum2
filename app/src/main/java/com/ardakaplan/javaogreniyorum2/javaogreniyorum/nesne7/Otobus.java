package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne7;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

// Otobus - MotorluTasit - Tasit
public class Otobus extends MotorluTasit {

    private int yukseklik = 2;

    public int getYukseklik() {
        return yukseklik;
    }

    public void setYukseklik(int yukseklik) {
        this.yukseklik = yukseklik;
    }


    @Override
    public void herhangiBirSeyYapanMethod() {

        int sayi = 3, sayi2 = 5;

        System.out.println(sayi + sayi2);

    }

    @Override
    public String toString() {
        return "Otobus{" +
                "yukseklik=" + yukseklik + super.toString() +
                '}';
    }
}
