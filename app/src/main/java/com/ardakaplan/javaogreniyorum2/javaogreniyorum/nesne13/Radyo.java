package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne13;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Radyo extends ElektrikliCihaz {


    @Override
    public void elektrikliCihazMetodu() {


    }

    // overload
    public void ekranaYazdir() {

    }

    public void ekranaYazdir(String data, String data2) {

    }

    public void ekranaYazdir(Integer data) {

    }
}
