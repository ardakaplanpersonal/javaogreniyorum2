package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne7;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Tasit extends Object {

    private String marka;
    private int tekelekSayisi;
    private int vitesSayisi;

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public int getTekelekSayisi() {
        return tekelekSayisi;
    }

    public void setTekelekSayisi(int tekelekSayisi) {
        this.tekelekSayisi = tekelekSayisi;
    }

    public int getVitesSayisi() {
        return vitesSayisi;
    }

    public void setVitesSayisi(int vitesSayisi) {
        this.vitesSayisi = vitesSayisi;
    }


    public void herhangiBirSeyYapanMethod() {

        System.out.println("Ben taşıtın içinden geldim");
    }

    @Override
    public String toString() {
        return "Tasit{" +
                "marka='" + marka + '\'' +
                ", tekelekSayisi=" + tekelekSayisi +
                ", vitesSayisi=" + vitesSayisi +
                '}';
    }
}
