package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne12;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public enum Not {

    PEK_IYI(5, "Pekiyi"),
    IYI(4, "İyi"),
    ORTA(3, "Orta"),
    KOTU(2, "Kötü"),
    ZAYIF(1, "Zayıf");

    private int rakamNotu;

    private String yazi;

    Not(int rakamNotu, String yazi) {
        this.rakamNotu = rakamNotu;
        this.yazi = yazi;
    }

    public int getRakamNotu() {
        return rakamNotu;
    }

    public String getYazi() {
        return yazi;
    }
}
