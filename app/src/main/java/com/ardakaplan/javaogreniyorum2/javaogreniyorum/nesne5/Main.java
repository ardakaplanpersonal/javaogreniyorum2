package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne5;

import com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne4.Doktor;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Doktor doktor1 = new Doktor("Ahmet Can", "Cildiye");
        Doktor doktor2 = new Doktor("Haldun Taner", "Diş");
        Doktor doktor3 = new Doktor("Hulusi Kentmen", "Genel Cerrah");

        Hasta hasta = new Hasta("Arda Kaplan");

        System.out.println(hasta);

        hasta.setDoktor(doktor1);

        System.out.println(hasta);

        //////////////////////////

        CokluHastalik cokHasta = new CokluHastalik("Ali Veli");


        Doktor[] doktorlar = {doktor2, doktor3};

        cokHasta.setDoktorlar(doktorlar);

        System.out.println(cokHasta);

    }
}
