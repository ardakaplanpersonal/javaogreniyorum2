package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne6;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Kisi {

    private String isim;
    private long tcKimlikNo;

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public long getTcKimlikNo() {
        return tcKimlikNo;
    }

    public void setTcKimlikNo(long tcKimlikNo) {
        this.tcKimlikNo = tcKimlikNo;
    }

    public void ekranaYazdir() {

        System.out.println("Merhaba ben bir kişiyim.");
    }

    @Override
    public String toString() {
        return "Kisi{" +
                "isim='" + isim + '\'' +
                ", tcKimlikNo=" + tcKimlikNo +
                '}';
    }
}
