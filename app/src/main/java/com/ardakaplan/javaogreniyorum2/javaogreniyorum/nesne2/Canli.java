package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne2;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Canli {

    private int kilo;
    private int boyu;
    private String isim;

    public static void ekranaİsminiYazdir(String isim) {

        System.out.println("Canlının ismi " + isim);
    }

    public int getKilo() {
        return kilo;
    }

    public void setKilo(int kilo) {
        this.kilo = kilo;
    }

    public int getBoyu() {
        return boyu;
    }

    public void setBoyu(int boyu) {
        this.boyu = boyu;
    }

    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    @Override
    public String toString() {
        return "Canli{" +
                "kilo=" + kilo +
                ", boyu=" + boyu +
                ", isim='" + isim + '\'' +
                '}';
    }

//    public void kiloDegistir(int kiloo) {
//
//        /*
//        admin girişi olsun
//
//        ancak admin değiştirebilsin
//
//         */
//        kilo = kiloo;
//    }
//
//    public int kiloyaEris() {
//
//        return kilo;
//    }
}
