package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne7;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Bisiklet extends Tasit {

    private int jantBoyu;

    public int getJantBoyu() {
        return jantBoyu;
    }


    public void setJantBoyu(int jantBoyu) {
        this.jantBoyu = jantBoyu;
    }

    @Override
    public String toString() {
        return "Bisiklet{" +
                "jantBoyu=" + jantBoyu + super.toString() +
                '}';
    }

    @Override
    public void herhangiBirSeyYapanMethod() {

        System.out.println("BEN BISIKLETTEN GELDIM");

    }
}
