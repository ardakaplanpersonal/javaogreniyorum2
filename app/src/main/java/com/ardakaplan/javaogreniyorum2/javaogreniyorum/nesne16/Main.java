package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne16;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        SingletonKisi singletonKisi = SingletonKisi.getInstance();

        System.out.println(singletonKisi.sayi);
    }
}
