package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne11;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Radyo extends ElektrikliCihaz {


    public void radyoMetodu() {

        ekranaYazdir();
    }
}
