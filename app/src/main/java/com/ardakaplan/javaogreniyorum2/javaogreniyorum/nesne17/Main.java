package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne17;

import java.util.ArrayList;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {


    public static void main(String[] args) {


//        //NULL POINTER EXCEPTION
//        Telefon telefon = new Telefon();
//        //çeşitli işlemler
//        telefon = null;
//
//        telefon.setMarka("Nokia");
//        System.out.println(telefon.getMarka());

//IndexOutOfBoundsException
        ArrayList<String> metinler = new ArrayList<>();

        metinler.add("asdas");
        metinler.add("asdas");
        metinler.add("asdas");
        metinler.add("asdas");
        metinler.add("asdas");


        try {

            metinler.get(15);

        } catch (IndexOutOfBoundsException e) {

            System.out.println("Sınırı aştınız, lütfen farklı bir index deneyin.");
        }


        metinler.get(15);


    }
}
