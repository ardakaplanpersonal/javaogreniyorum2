package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne10;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Brokoli extends Sebze {
    
    public Brokoli(String isim) {
        super(isim);
    }
}
