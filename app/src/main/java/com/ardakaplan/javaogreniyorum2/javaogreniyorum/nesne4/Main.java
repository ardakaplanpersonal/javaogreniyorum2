package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne4;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Doktor doktor1 = new Doktor("Ahmet Can", "Cildiye");

        System.out.println(doktor1);

        Doktor doktor2 = new Doktor("Doktor 2");

        System.out.println(doktor2);

        Doktor doktor3 = new Doktor();
        doktor3.setIsmi("doktor 3");
        doktor3.setUzmanligi("diş");

        System.out.println(doktor3);
    }
}
