package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne2;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Canli canli = new Canli();

        canli.setIsim("Mehmet");
        canli.setKilo(23);
        canli.setBoyu(150);

        Canli.ekranaİsminiYazdir(canli.getIsim());

        canli.ekranaİsminiYazdir(canli.getIsim());


        System.out.println(canli);

    }
}
