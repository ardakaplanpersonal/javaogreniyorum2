package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne8;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Hayvan extends Canli {

    public Hayvan(int kutle, String isim) {
        super(kutle, isim);
    }

    @Override
    public void buyumek() {

        System.out.println(getIsim() + " 10 cm büyüdü.");

        setKutle(getKutle() + 50);
    }
}
