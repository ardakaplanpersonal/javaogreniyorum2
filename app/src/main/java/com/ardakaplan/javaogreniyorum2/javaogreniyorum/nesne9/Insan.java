package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne9;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Insan implements YemekYer {

    private String isim;


    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }

    public void yuru(int adimSayisi) {

        System.out.println(isim + " " + adimSayisi + " adim attı");

    }

    @Override
    public void yemekYe() {

        System.out.println(isim + " acıktı ve yemek yedi.");
    }
}
