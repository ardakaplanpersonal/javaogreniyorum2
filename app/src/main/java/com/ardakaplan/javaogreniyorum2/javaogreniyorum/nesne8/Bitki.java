package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne8;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Bitki extends Canli {

    public Bitki(int kutle, String isim) {
        super(kutle, isim);
    }

    @Override
    public void buyumek() {

        System.out.println(getIsim() + " 1 cm büyüdü.");

        setKutle(getKutle() + 5);
    }
}
