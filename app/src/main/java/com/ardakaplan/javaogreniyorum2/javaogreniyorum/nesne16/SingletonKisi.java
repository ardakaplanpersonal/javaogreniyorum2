package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne16;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class SingletonKisi {

    private static SingletonKisi singletonKisi;

    public int sayi = 19;

    private SingletonKisi() {

    }

    public static SingletonKisi getInstance() {

        if (singletonKisi == null) {

            singletonKisi = new SingletonKisi();
        }

        return singletonKisi;
    }
}