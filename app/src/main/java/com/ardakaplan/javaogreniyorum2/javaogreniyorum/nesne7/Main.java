package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne7;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        Araba araba1 = new Araba();
        araba1.setTipi("Sedan");
        araba1.setMarka("BMW");
        araba1.setMotorHacmi(1500);
        araba1.setTekelekSayisi(4);
        araba1.setVitesSayisi(5);
        System.out.println(araba1);

        Araba araba2 = new Araba();
        araba2.setTipi("Sedan");
        araba2.setMarka("BMW");
        araba2.setMotorHacmi(1500);
        araba2.setTekelekSayisi(4);
        araba2.setVitesSayisi(5);
        System.out.println(araba2);

        System.out.println(araba1.equals(araba2));


        Bisiklet bisiklet = new Bisiklet();
        bisiklet.setJantBoyu(18);

        Otobus otobus = new Otobus();
        otobus.setYukseklik(10);

        Kisi kisi = new Kisi("Celal Bayer");
        Tasit tasitlar[] = {araba1, araba2, bisiklet, otobus};
        kisi.setTasitlar(tasitlar);
        System.out.println(kisi);

        System.out.println();
        System.out.println();

        for (int i = 0; i < tasitlar.length; i++) {

            tasitlar[i].herhangiBirSeyYapanMethod();
        }
    }
}
