package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne6;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Avukat extends Kisi {

    private int bakdıgıDavaSayisi;

    public int getBakdıgıDavaSayisi() {
        return bakdıgıDavaSayisi;
    }

    public void setBakdıgıDavaSayisi(int bakdıgıDavaSayisi) {
        this.bakdıgıDavaSayisi = bakdıgıDavaSayisi;
    }

    @Override
    public void ekranaYazdir() {

        System.out.println("Merhaba ben bir avukatım.");
    }

    @Override
    public String toString() {
        return "Avukat{" +
                "bakdıgıDavaSayisi=" + bakdıgıDavaSayisi + super.toString() +
                '}';
    }
}
