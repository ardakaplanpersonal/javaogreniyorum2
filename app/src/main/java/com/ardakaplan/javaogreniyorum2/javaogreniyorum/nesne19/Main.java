package com.ardakaplan.javaogreniyorum2.javaogreniyorum.nesne19;

import java.util.ArrayList;

/**
 * Created by Arda Kaplan on
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Main {

    public static void main(String[] args) {

        ArrayList<String> liste = new ArrayList<>();


        System.out.println("integer dizisi yazdırılıyor");
        Integer[] intDizisi = {1, 2, 3, 4, 5};
        listeElemanlariniYazdir(intDizisi);

        System.out.println("string dizisi yazdırılıyor");
        String[] stringDizisi = {"asdas", "asdasd", "1asdasdas", "asdasd"};
        listeElemanlariniYazdir(stringDizisi);
    }


    public static <W> void listeElemanlariniYazdir(W[] dizi) {

        for (int i = 0; i < dizi.length; i++) {

            System.out.println(dizi[i]);
        }

    }
}
